#!/bin/bash

cd ../helm/service-nodejs

APIKEY="AKCp5buTzKgciJPQEbPxjhrevqkhyyRrD33nRDXs4C7DDByXH9DDjWA7SaCYzre8VtQLsbR5e"

NAME=$(sed -n 's/^name: \(.*\)/\1/gp' Chart.yaml)
VERSION=$(sed -n 's/^version: \(.*\)/\1/gp' Chart.yaml)

FILE="${NAME}-${VERSION}.tgz"

helm dependency build
helm package .

curl -vvv -H "X-JFrog-Art-Api:${APIKEY}" \
	-H "X-CheckSum-Sha256: $(sha256sum ${FILE} | awk '{ print $1 }')" \
        -H "X-Checksum-Sha1: $(sha1sum ${FILE} | awk '{ print $1 }')" \
	-H "X-Checksum: $(md5sum ${FILE} | awk '{ print $1 }')" \
	-T ${FILE} "https://repo.infrontfinance.dev/artifactory/helm-templates/microservices/nodejs/${NAME}/${FILE}"
