{{/* vim: set filetype=mustache: */}}
{{/*
   Renders a value that contains template.
  Usage:
  {{ include "common.tplvalues.render" ( dict "value" .Values.path.to.the.Value "context" $) }}
*/}}
{{- define "common.tplvalues.render" -}}
  {{- if typeIs "string" .value }}
    {{- tpl .value .context }}
  {{- else }}
    {{- tpl (.value | toYaml) .context }}
  {{- end }}
{{- end -}}

{{/*
   Inlines a vault secret to be able to use multiple secrets in a string.
  Usage:
  {{ include "inlinesecret" ( dict "value" .Values.path.to.the.Value "context" $) }}
*/}}
{{- define "inlinesecret" -}}
  {{- if hasPrefix "vault:" .value -}}
    {{- print "${" .value "}" -}}
  {{- else -}}
    {{- print .value -}}
  {{- end -}}
{{- end -}}
