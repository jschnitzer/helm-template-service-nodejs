apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ required "A value for service.name is required!" .Values.service.name }}
spec:
  replicas: {{ .Values.replicas | default "1" }}

  strategy:
    type:  {{ .Values.updateStrategy | default "RollingUpdate" }}
    rollingUpdate:
      maxUnavailable: 20%
      maxSurge: 20%

  selector:
    matchLabels:
      app: {{ .Values.service.name }}
      #release: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app: {{ .Values.service.name }}
        #release: {{ .Release.Name }}
      annotations:
        # Explicitely take vault values only when defined and don't specify defaults here
        # because defaults are partly cluster dependent and specified in the vault webhook deployment
        {{- if .Values.service.vault }}
        {{- if .Values.service.vault.url }}
        vault.security.banzaicloud.io/vault-addr: {{ .Values.service.vault.url }}
        {{- end }}
        {{- if .Values.service.vault.envFromPath }}
        vault.security.banzaicloud.io/vault-env-from-path: {{ .Values.service.vault.envFromPath }}
        {{- end }}
        {{- if .Values.service.vault.path }}
        vault.security.banzaicloud.io/vault-path: {{ .Values.service.vault.path }}
        {{- end }}
        {{- if .Values.service.vault.authMethod }}
        vault.security.banzaicloud.io/vault-auth-method: {{ .Values.service.vault.authMethod }}
        {{- end }}
        {{- if .Values.service.vault.role }}
        vault.security.banzaicloud.io/vault-role: {{ .Values.service.vault.role }}
        {{- end }}
        {{- end }}
        technology_group: nodejs
        {{- if .Values.podAnnotations }}
{{ toYaml .Values.podAnnotations | indent 8 }}
        {{- end }}
    spec:
      affinity:
      {{- if .Values.affinity }}
{{ toYaml .Values.affinity | indent 8 }}
      {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 1
              podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: "app"
                      operator: In
                      values:
                        - {{ .Values.service.name }}
#                    - key: "release"
#                      operator: In
#                      values:
#                        - {{ .Release.Name }}
                topologyKey: "kubernetes.io/hostname"
      {{- end }}
      {{- if .Values.initialization }}
      initContainers:
        {{- if .Values.initialization.extraContainers }}
        {{- include "common.tplvalues.render" (dict "value" .Values.initialization.extraContainers "context" $) | nindent 8 }}
        {{- end }}
        {{- if .Values.initialization.kafkaTopicCreationCmd }}
        - name: kafka-create-topics
          image: confluentinc/cp-kafka:{{ .Values.initialization.kafkaTopicCreationImageTag | default "6.1.1" }}
          command: ['bash', '-c', 'cub kafka-ready -b {{ .Values.initialization.kafkaTopicCreationBroker  | default "kafka-central-cp-kafka-headless:9092" }} 3 300 &&
          {{ .Values.initialization.kafkaTopicCreationCmd }}']
          env:
            - name: KAFKA_BROKER_ID
              value: "ignored"
            - name: KAFKA_ZOOKEEPER_CONNECT
              value: "ignored"
            - name: KAFKA_AUTO_CREATE_TOPICS_ENABLE
              value: "false"
        {{- end }}
      {{- end }}
      containers:
      - name: {{ .Values.service.name }}
        image: {{ .Values.image.repository | default "vauwede-docker.jfrog.io" }}/{{ .Values.image.name | default .Values.service.name }}:{{ required "A value for image.tag is required!" .Values.image.tag }}
        livenessProbe:
          httpGet:
            path: /management/info
            port: {{ .Values.service.portREST | default "8080" }}
          failureThreshold: 3
          successThreshold: 1
          initialDelaySeconds: 30
          periodSeconds: 30
          timeoutSeconds: 5
        readinessProbe:
          httpGet:
            path: /management/info
            # REST direct into service without sidecar
            port: {{ .Values.service.portREST | default "8080" }}
          failureThreshold: 3
          successThreshold: 1
          initialDelaySeconds: 30
          periodSeconds: 30
          timeoutSeconds: 5
        {{- if eq ( .Values.service.hasDB | default false ) true }}
        {{- if eq ( .Values.service.startDBAsContainer | default false ) true }}
        envFrom:
          - secretRef:
              name: db-{{ kebabcase .Values.db.name | default .Values.service.name }}
        {{- end }}
        {{- end }}
        env:
          #clarify handling container_env
#          - name: CONTAINER_ENV
#            value: staging
          - name: SERVICE_TYPE
          {{- if eq ( .Values.service.hasGRPC | default false ) true }}
            value: grpc
          - name: SERVICE_PORT
            value: {{ .Values.service.portGRPC | default "10000" | quote }}
          {{- else }}
            value: rest
          - name: SERVICE_PORT
            value: {{ .Values.service.portREST | default "8080" | quote }}
          {{- end }}

          # TODO - align env vars or not?
#          - name: SERVER_PORT_REST
#            value: {{ .Values.service.portREST | default "8080" | quote }}
#          - name: SERVER_PORT_GRPC
#            value: {{ .Values.service.portGRPC | default "10000" | quote }}
#          - name: TLS_ENABLED
#            value: {{ .Values.service.tlsEnabled | default "false" | quote }}
#          - name: LOGBACK_LOGLEVEL
#            value: {{ .Values.service.logLevel | default "ERROR" | quote }}
          # END TODO

          - name: NODE_ENABLE_HTTPS
            value: {{ .Values.service.tlsEnabled | default false | quote }}
          - name: NODE_TLS_REJECT_UNAUTHORIZED
            value: {{ .Values.service.tlsRejectUnauthorized | default "0" | quote }}
          {{- if eq ( .Values.service.tlsEnabled | default false ) true }}
          - name: SSL_CERT
            value: keystore.p12
          - name: SSL_PASSPHRASE
            value: changeit
          {{- end}}

          # TODO
          {{- if eq ( .Values.service.hasMemcached | default true ) true }}
          - name: MEMCACHED_URL
            value: {{ .Values.memcachedUrl | default "vwd-memcached:11211" | quote }}
          {{- end}}
          # END TODO

          {{- if eq ( .Values.service.hasDB | default false ) true }}
          # TODO - align
          - name: MYSQL_CRED
            value: "{{ include "inlinesecret" ( dict "value" (.Values.db.user | default .Values.service.name) "context" $ ) }}:{{ include "inlinesecret" ( dict "value" (required "A value for db.pwd is required!" .Values.db.pwd) "context" $ ) }}:{{ .Values.db.name | default .Values.service.name }}"
          - name: MYSQL_URL
            value: db-{{ kebabcase .Values.db.name | default .Values.service.name }}:{{ .Values.db.port | default "3306" }}
          - name: DB_HOST
            value: db-{{ .Values.db.host | default .Values.service.name }}
          - name: DB_NAME
            value: {{ .Values.db.name | default .Values.service.name | quote }}
          {{- if eq ( .Values.service.startDBAsContainer | default false ) false }}
          - name: DB_USER
            value: {{ .Values.db.user | default .Values.service.name | quote }}
          - name: DB_PWD
            value: {{ required "A value for db.pwd is required!" .Values.db.pwd | quote }}
          {{- end }}
          {{- end }}
          {{- if eq ( .Values.service.hasNATS | default true ) true }}
          - name: NATS_URL
            value: "nats-node:4222"
          - name: NATS_SERVERS
            value: "[\"nats://nats-node:4222\"]"
          {{- end}}
          {{- if eq ( .Values.service.hasKafka | default false ) true }}
          {{- if .Values.kafka }}
          # TODO
          - name: KAFKA_URLS
            value: "kafka-central-cp-kafka-headless:9092"
          # END TODO - how do you set this defaults?
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_AUTO_CREATE_TOPICS
            value: {{ .Values.kafka.autoCreateTopics | default "false" | quote }}
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_BROKERS
            value: {{ .Values.kafka.brokers | default "kafka-central-cp-kafka-headless:9092" | quote }}
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_DEFAULT_PARTITIONS
            value: {{ .Values.kafka.defaultPartitions | default "10" | quote }}
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_REPLICATION_FACTOR
            value: {{ .Values.kafka.defaultReplicationFactor | default "3" | quote }}
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_ZK_NODES
            value: {{ .Values.kafka.zookeeperNodes | default "kafka-central-cp-zookeeper-headless:2181" | quote }}
          {{- else }}
          # TODO
          - name: KAFKA_URLS
            value: "kafka-central-cp-kafka-headless:9092"
          # END TODO
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_AUTO_CREATE_TOPICS
            value: "false"
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_BROKERS
            value: "kafka-central-cp-kafka-headless:9092"
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_DEFAULT_PARTITIONS
            value: "10"
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_REPLICATION_FACTOR
            value: "3"
          - name: SPRING_CLOUD_STREAM_KAFKA_BINDER_ZK_NODES
            value: "kafka-central-cp-zookeeper-headless:2181"
          {{- end}}
          {{- end}}
          {{- range $key, $value := .Values.customEnv }}
          - name: {{ $key | quote }}
            value: {{ $value | quote }}
          {{- end }}
        {{- if .Values.customVolumeMounts }}
        volumeMounts:
          {{- range $volumeMount := .Values.customVolumeMounts }}
          - {{ $volumeMount | toJson }}
          {{- end }}
        {{- end}}
        resources:
          {{- if .Values.resources }}
{{ toYaml .Values.resources | indent 10 }}
          {{- else}}
          limits:
            cpu: "1200m"
            memory: 1024Mi
          requests:
            cpu: "50m"
            memory: 512Mi
          {{- end}}
        imagePullPolicy: {{ .Values.image.pullPolicy | default "IfNotPresent" }}
      {{- if eq ( .Values.service.hasSidecarEnvoy | default false ) true }}
      ###
      ### Envoy Sidecar
      ###
      - name: {{ .Values.service.name }}-sidecar-envoy
        image: {{ .Values.image.repository | default "vauwede-docker.jfrog.io" }}/{{ .Values.image.name | default .Values.service.name }}-sidecar-envoy:{{ required "A value for image.tag is required!" .Values.image.tag }}
        env:
          - name: SERVICE_HOST
            value: "127.0.0.1"
          - name: SERVICE_PORT
            value: {{ .Values.service.portGRPC | default "10000" | quote }}
          - name: ENVOY_PORT_HTTP
            value: {{ .Values.envoy.portREST | default "7443" | quote }}
          - name: ENVOY_PORT_GRPC
            value: {{ .Values.envoy.portGRPC | default "20000" | quote }}
          - name: ENVOY_UPSTREAM_TIMEOUT
            value: {{ .Values.envoy.upstream_timeout | default "30" | quote }}
          - name: ENVOY_IDLE_TIMEOUT
            value: {{ .Values.envoy.idle_timeout | default "300" | quote }}
          - name: ENVOY_CONVERT_GRPC_STATUS
            value: {{ .Values.envoy.convert_grpc_status | default "true" | quote }}
        livenessProbe:
          exec:
            command:
              - sh
              - -c
              - /healthcheck.sh
          failureThreshold: 3
          successThreshold: 1
          initialDelaySeconds: 10
          periodSeconds: 5
          timeoutSeconds: 2
        readinessProbe:
          exec:
            command:
              - sh
              - -c
              - /healthcheck.sh
          failureThreshold: 3
          successThreshold: 1
          initialDelaySeconds: 10
          periodSeconds: 5
          timeoutSeconds: 2
        resources:
          limits:
            cpu: "800m"
            memory: 256Mi
          requests:
            cpu: "50m"
            memory: 64Mi
        imagePullPolicy: {{ .Values.image.pullPolicy | default "IfNotPresent" }}
      {{- end}}
      {{- if .Values.sidecars }}
      {{- include "common.tplvalues.render" ( dict "value" .Values.sidecars "context" $) | nindent 6 }}
      {{- end }}
      restartPolicy: {{ .Values.restartPolicy | default "Always" }}
      imagePullSecrets:
        - name: regcred
      {{- if .Values.customVolumes }}
      volumes:
        {{- range $volumeDefinition := .Values.customVolumes }}
        - {{ $volumeDefinition | toJson }}
        {{- end }}
      {{- end}}
