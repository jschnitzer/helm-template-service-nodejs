# Helm Template Service Nodejs

This is a helm template for a standard Nodejs microservice.

## Introduction

This chart bootstraps a [Nodejs Service](https://bitbucket.org/vauwede/helm-template-service-nodejs/) deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Prerequisites

- Kubernetes 1.12+
- Helm 3.1.0

## Installing the Chart

To install the chart with the release name `my-release` (in chart directory):

```bash
$ helm install my-release .
```

These command deploys a that chart on the Kubernetes cluster in the default configuration.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

The following tables lists the configurable parameters of the chart and their default values per section/component:

### Global parameters

In best case if you kept with our standards you only have to specify `service.name`.

| Parameter                         | Description                                                                                                                                                   | Default                                                 |
|-----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| `replicas` | The number of replicas for that pod | "1" |
| `updateStrategy` | The update strategy when updating the service (either "RollingUpdate" or "Recreate") | "RollingUpdate" |
| `podAnnotations` | Annotations for the service pods | `{}`                           |
| `affinity` | Affinity for pod assignment | `{}` By default the chart tries to distribute the pods over the nodes with `podAntiAffinity` `preferredDuringSchedulingIgnoredDuringExecution`. If you specify affinity only your definitions will be taken. |
| `restartPolicy` | Restart policy for the pod (Always, OnFailure or Never) | "Always" |
| `sidecars` | Define here if you need additional sidecar containers to the pods | `{}` (evaluated as a template) |

### Initialization parameters

| Parameter                         | Description                                                                                                                                                   | Default                                                 |
|-----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| `initialization.kafkaTopicCreationCmd` | If you need to initialize kafka topics with an init container you simple have to specify your kafka creation command here, see sample below. | |
| `initialization.kafkaTopicCreationImageTag` | If you need to override tag of the confluent image used for topic creation specify here. | "6.1.1" |
| `initialization.initContainers` | If you need add additional init containers to the service pods here | `{}` (evaluated as a template) |

Sample for kafkaTopicCreationCmd:

```yaml
initialization:
  kafkaTopicCreationCmd: |-1
    kafka-topics --create --topic output --if-not-exists --bootstrap-server kafka-central-cp-kafka-headless:9092 --partitions 10 --replication-factor 3 &&
    kafka-topics --create --topic idm --if-not-exists --bootstrap-server kafka-central-cp-kafka-headless:9092 --partitions 10 --replication-factor 3
```

### Service parameters

In best case if you kept with our standards you only have to specify `service.name`.

| Parameter                         | Description                                                                                                                                                   | Default                                                 |
|-----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| `service.name` | Name of the service (automatically also the default for the image name)  ||
| `service.tlsEnabled` | If you want to enable tls (don't do without a reason) set this flag to true | "false" |
| `service.logLevel` | Log level of the service | "ERROR" |
| `service.hasGRPC` | Set to true if service has grpc endpoints | false |
| `service.hasSidecarEnvoy` | Set to true if service has grpc endpoints and wants an automatic REST transition | false |
| `service.portREST` | The port for the REST endpoints | 8080 |
| `service.portGRPC` | The port for the GRPC endpoints (if available) | 10000 |
| `service.hasKafka` | Set to true if service uses Kafka as a dependency (see also Kafka parameters) | false |
| `service.hasNATS` | Set to true if service uses NATS. It sets the necessary connection env variables. | true |
| `service.hasDB` | Set to true if service uses a database (see also database parameters) | false |
| `service.startDBAsContainer` | Set to true if service wants to use an internal database which comes up with this chart | false |
| `service.hasPrometheus` | Set to true if service exposes prometheus metrics (see also metrics parameters) | false |
| `image.registry` | The image registry where to pull the containers | "vauwede-docker.jfrog.io" |
| `image.name` | Only specify this container image name override if not identical to service.name. | service.name |
| `image.tag` | The tag of the service container used in this chart release (normally managed by Jenkins or Flux) | "latest" |
| `image.pullPolicy` | Image pull policy of the service container | "IfNotPresent" |
| `customEnv` | Extra environment variables to be set on the service container (see example below in chapter "Adding extra environment variables") | `{}`                                                    |
| `resources` | Resource requests and limits defined for the service container if you need something different from the defaults. | `{}` By default the chart requests 50m cpu and 512M RAM and limits to 1200m cpu and 1024M RAM which should fit as a standard. If you specify resources only your definitions will be taken. |

### Metrics parameters

| Parameter                                 | Description                                                                         | Default                                                      |
|-------------------------------------------|-------------------------------------------------------------------------------------|--------------------------------------------------------------|
| `prometheus.scrape` | Enables or disables scraping by adding the prometheus.io/scrape annotation | "true" |
| `prometheus.scheme` | Specifies the scheme for scraping by adding the prometheus.io/scheme annotation | "http" |
| `prometheus.path` | Specifies the path where to retrieve the metrics by adding the prometheus.io/path annotation | "/management/prometheusMetrics/metrics" |
| `prometheus.port` | Specifies the port of the metrics endpoint by adding the prometheus.io/port annotation | service.portREST |
| `prometheus.username` | Specifies the basic auth username of the metrics endpoint by adding the prometheus.io/username annotation | "" |
| `prometheus.password` | Specifies the basic auth password of the metrics endpoint by adding the prometheus.io/password annotation | "" |

### Database parameters

| Parameter                        | Description                                                                  | Default            |
|----------------------------------|------------------------------------------------------------------------------|--------------------|
| `db.host` | IP of the external database (only specify if hasDB true and startDBAsContainer false) | |
| `db.name` | Name of the db if you want to override the default which is the service name (final name is db-<db.name>) | service.name |
| `db.port` | Name of the db port if you want to override the default | 3306 |
| `db.user` | Database user name - required for external database, not used for internal database. Must be a vault secret reference like "vault:cloud_staging/data/service/sample-deploy-group/nodejs-sample-service#DB_USER" | service.name |
| `db.pwd` | Database password - required for external database, not used for internal database.  Must be a vault secret reference like "vault:cloud_staging/data/service/sample-deploy-group/nodejs-sample-service#DB_PWD" |  |

### Kafka parameters

| Parameter                        | Description                                                                  | Default            |
|----------------------------------|------------------------------------------------------------------------------|--------------------|
| `kafka.brokers` | Address of the Kafka brokers to connect to. Defaults to the central Kafka cluster. | "kafka-central-cp-kafka-headless:9092" |
| `kafka.zookeeperNodes` | Address of the Zookeeper nodes to connect to. Defaults to the central Kafka cluster. | "kafka-central-cp-zookeeper-headless:2181" |
| `kafka.autoCreateTopics` | Flag which specifies if automatic topic creation is enabled. Set this to true if used for local development. | "kafka-central-cp-kafka-headless:9092" |
| `kafka.defaultPartitions` | The number of default partitions which will be created for a topic. | "10" |
| `kafka.defaultReplicationFactor` | The default replication factor which will be used on topic creation. Set to 1 for local testing. | "3" |

### Vault parameters

Normally you don't have to specify these values. Only do that if you know what you do and explicitely don't want to
have our defaults which are specified with the vault webhook.

| Parameter                         | Description                                                                                                                                                   | Default                                                 |
|-----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| `service.vault.url` | Url of the vault host (annotation vault.security.banzaicloud.io/vault-addr) | default of vault webhook |
| `service.vault.path` | Specifies the value of the annotation vault.security.banzaicloud.io/vault-path | default of vault webhook |
| `service.vault.authMethod` | Specifies the value of the annotation vault.security.banzaicloud.io/vault-auth-method | default of vault webhook |
| `service.vault.role` | Specifies the value of the annotation vault.security.banzaicloud.io/vault-role | default of vault webhook |
| `service.vault.envFromPath` | Specifies the value of the annotation vault.security.banzaicloud.io/vault-env-from-path | default of vault webhook |

### Envoy sideacar parameters

| Parameter                        | Description                                                                  | Default            |
|----------------------------------|------------------------------------------------------------------------------|--------------------|
| `envoy.portREST` | The port for the Envoy sidecar REST endpoint (gRPC-JSON transcoder). Must be different from service.portREST and service.portGRPC! | 7443 |
| `envoy.portGRPC` | The port for the Envoy sidecar GRPC endpoint. Must be different from service.portREST and service.portGRPC! | 20000 |
| `envoy.upstream_timeout` | The upstream timeout for requests to the gRPC service in seconds | 30 |
| `envoy.idle_timeout` | The idle timeout for the route in seconds | 300 |
| `envoy.convert_grpc_status` | Automatically add gRPC error details to HTTP body if body is empty | true |

### RBAC parameters

| Parameter               | Description                                               | Default                                          |
|-------------------------|-----------------------------------------------------------|--------------------------------------------------|

### Other parameters

| Parameter                  | Description                                                    | Default |
|----------------------------|----------------------------------------------------------------|---------|

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```bash
helm install my-release --set service.name=sample-service .
```

The above command sets the service name to `sample-service`.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```bash
$ helm install my-release -f values.yaml .
```

> **Tip**: You can use the default [values.yaml](values.yaml)

## Minimal configuration

Here you see an example of a minimal configuration for an service. If you follow all standards this should be all.

```yaml
service-nodejs:

  service:
    name: nodejs-sample-service
    hasGRPC: true
    hasSidecarEnvoy: true
    hasDB: true
    hasKafka: true
    hasPrometheus: true

  initialization:
    kafkaTopicCreationCmd: |-1
      kafka-topics --create --topic output --if-not-exists --bootstrap-server kafka-central-cp-kafka-headless:9092 --partitions 10 --replication-factor 3 &&
      kafka-topics --create --topic idm --if-not-exists --bootstrap-server kafka-central-cp-kafka-headless:9092 --partitions 10 --replication-factor 3

  image:
    tag: latest

  db:
    host: "10.10.128.134"
    user: "vault:cloud_staging/data/service/sample-deploy-group/nodejs-sample-service#DB_USER"
    pwd: "vault:cloud_staging/data/service/sample-deploy-group/nodejs-sample-service#DB_PWD"
```

## Configuration and installation details

### Using an internal or external database

If your service need a database you have to set  `service.hasDB` parameter to true.

For testing you might want to deploy and internal database with that chart. For Production use-case you further more
refer to a existing prod database with HA setup.

You decide that with the `service.startDBAsContainer` parameter. Set this to true for an internal database.

If you want to use an external database omit  `service.startDBAsContainer` or set it to false.

If you use an external database you have to specify the host of the db:

```yaml
  service:
    name: sample-service
    hasDB: true

  db:
    host: "10.10.128.134"
```

### Adding extra environment variables

In case you want to add extra environment variables (useful for advanced operations like custom init scripts), you can use the `customEnv` property.

```yaml
  customEnv:
    TEST_ENV: "test-value"
    NEXT_ENV: "next-value"
```

Alternatively, you can use a ConfigMap or a Secret with the environment variables. To do so, use the `extraEnvVarsCM` or the `extraEnvVarsSecret` values.

### Sidecars and Init Containers

If you have a need for additional containers to run within the same pod as the Keycloak app (e.g. an additional metrics or logging exporter), you can do so via the `sidecars` config parameter. Simply define your container according to the Kubernetes container spec.

```yaml
sidecars:
  - name: your-image-name
    image: your-image
    imagePullPolicy: Always
    ports:
      - name: portname
       containerPort: 1234
```

Similarly, you can add extra init containers using the `initialization.extraContainers` parameter.

```yaml
initialization:
  extraContainers:
    - name: your-image-name
      image: your-image
      imagePullPolicy: Always
      ports:
        - name: portname
          containerPort: 1234
```

### Deploying extra resources

There are cases where you may want to deploy extra objects, such a ConfigMap containing your app's configuration or some extra deployment with a micro service used by your app. For covering this case, the chart allows adding the full specification of other objects using the `extraDeploy` parameter.

### Setting Pod's affinity

This chart allows you to set your custom affinity using the `affinity` parameter. Find more information about Pod's affinity in the [kubernetes documentation](https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity).

By default the chart tries to distribute the pods over the nodes with `podAntiAffinity` `preferredDuringSchedulingIgnoredDuringExecution`.

### Secrets and passwords

With this chart you get access to vault for managing secrets. Please create them in vault and reference it.

Please find the syntax for referencing in the following sample. The value gets automatically injected in to the env variable.

For example:
```yaml
  customEnv:
    DB_PWD: vault:cloud_staging/data/service/auth/sample-service#DB_PWD
```

> NOTE: The vault webhook must be installed in the cluster for successful injection.

## Change requests

Please contact maintainers of this chart for required changes.

## Release changes

* 0.7.0 Revert changes to ports, add envoy options
  service.portREST always is the REST port of the service itself
  envoy REST port is 7443 by default (adjust via envoy.portREST)
  envoy GRPC port is 20000 by default (adjust via envoy.portGRPC)
  Added options to set envoy timeouts and configure how error details are transported
* 0.6.8 Changed port assignments
  hasSidecarEnvoy true -> service.portREST is the envoy port for REST transition, service port for REST management endpoints moves to 18080  
  hasSidecarEnvoy false -> service.portREST is the direct REST endpoint for the service itself (no envoy sidecar involved)
* 0.6.7 Introduced envoy sidecar (new value service.hasSidecarEnvoy)
* 0.5.4 Changing to probes from kubelet instead of curl (no curl in image needed)

## Upgrading

### To 1.0.0

**What changes were introduced in this major version?**

- TO-DO

**Considerations when upgrading to this version**

- TO-DO

> NOTE: Please, create a backup of your database before running any of those actions.

##### Upgrade the chart release

```console
$ helm upgrade my-release .
```

**Useful links**

- https://kubernetes.io/docs/concepts/
- https://helm.sh/docs/
- https://banzaicloud.com/docs/bank-vaults/mutating-webhook/configuration/