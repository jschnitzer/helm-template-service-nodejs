#!/usr/bin/env bash

clear
cd service-nodejs-minimal
helm dep update
helm template njs-starter . --debug
read -n 1
