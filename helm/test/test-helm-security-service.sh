#!/usr/bin/env bash

clear
cd service-nodejs-minimal
helm dep update
helm template njs-starter . -f values-security-service.yaml --debug
read -n 1
